* General information
- to check inlined images run:
#+begin_src elisp
;; run C-x C-e on command line
  (org-toggle-inline-images)
#+end_src

* How to run postmarketos in qemu
** clone postmarket bootstrap
#+begin_src bash
  $ git clone https://gitlab.com/postmarketOS/pmbootstrap
#+end_src
** initialize config file
   - select *qemu-amd64* machine
   - select *posh* UI
   - select *alpine* linux base
#+begin_src bash
  $ python pmbootstrap.py init
#+end_src
** (optional?) update
#+begin_src bash
  $ python pmbootstrap.py update
  $ python pmbootstrap.py pull
#+end_src
** set up device chroot environment
#+begin_src bash
  $ python pmbootstrap.py install
#+end_src
** run qemu
#+begin_src bash
  $ python pmbootstrap.py qemu
  # in case of problems with display you can try:
  $ python pmbootstrap.py qemu --display sdl
#+end_src
** postrun remarks
   - as the pin password just type your admin pass
** deploy pms phosh on pine64 phone
   preparing microSD using installer image perfectly works
   https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)

** result
#+ CAPTION: Running qemu with pms
#+ NAME: fig:qemu-pms
[[./pms_qemu.png]]

** reference links
- https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)
- https://wiki.postmarketos.org/wiki/Phosh
