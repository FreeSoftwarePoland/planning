# FSF Poland
Witamy w Polskiej Społeczności Free Software.

## Unofficial group
## Nieoficjalna grupa sympatyków

## Current goals (focus areas)
## Aktualne plany co do działalności
- [ ] tworzymy pierwszą aplikacje Gtk na telefon
- [ ] strona dla FSF Polska
- [ ] logo dla FSF Polska
- [ ] pomysły?

## Contact
- IRC
server: Libera.Chat
chanel: #fsf-pl

- Telegram
channel: https://t.me/free_software_poland

- LibrePlanet
Dopisujemy się do grupy
https://libreplanet.org/wiki/Group:Poland#B.C4.85d.C5.BA_z_nami
